﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;


namespace Lab1.Implementation
{
    class Impl1 : IGoryl
    {
        public Impl1()
        { }

        public string PrzedstawSie()
        {
            return "nazywam się goryl";
        }

        public string Władza()
        {
            return "odbiram wam władze";
        }

        string IGoryl.Władza()
        {
            return "nazywam sie Goryl";
        }

        string IMalpa.PrzedstawSie()
        {
            return "";
        }
    }

    class Impl2 : IPawian
    {
        public Impl2() { }

        public string PrzedstawSie()
        {
            return "witam was";
        }

        public void Relax()
        {
            Console.WriteLine("mam teraz odpoczynek");
        }

        string IPawian.Relax()
        {
            return "odpoczywam";
        }
        string IMalpa.PrzedstawSie()
        {
            return "jestem Pawian";
        }
    }



    class Impl3 : IMalpa, IOlbrzym, IGoryl
    {
        public Impl3()
        {

        }

        string IMalpa.PrzedstawSie()
        {
            return "metoda1";
        }

        string IOlbrzym.PrzedstawSie()
        {
            return "metoda2";
        }

        string IOlbrzym.Inny()
        {
            return "metoda3";
        }

        public void PrzedstawSie()
        {

        }

        public string Władza()
        {
            return "";
        }
    }
}
