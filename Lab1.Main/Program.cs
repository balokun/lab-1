﻿using System;
using Lab1.Implementation;
using Lab1.Contract;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            IList<IMalpa> lista = program.StworzListe();

            program.PrzedstawSie(lista);

            Console.ReadKey();
        }

        public IList<IMalpa> StworzListe()
        {
            List<IMalpa> tablica = new List<IMalpa>() 
            { 
                new Impl1(), new Impl2()
            };

            return tablica;
        }

        public void PrzedstawSie(IList<IMalpa> tablica)
        {
            foreach (IMalpa malpa in tablica)
            {
                Console.WriteLine(malpa.PrzedstawSie());
            }
        }
    }
}
