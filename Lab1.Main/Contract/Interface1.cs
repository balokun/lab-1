﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    public interface IMalpa
    {
        string PrzedstawSie();
    }

    public interface IGoryl : IMalpa
    {
        string Władza();
    }

    public interface IPawian : IMalpa
    {
        string Relax();
    }

    public interface IOlbrzym
    {
        string PrzedstawSie();
        string Inny();
    }
}
